$(function(){
    // $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
        interval: 5000
    });

    // las propiedades solo cambian con el botón de "Hotel San Pablo" Importante para ver cambios de class CSS
    $('#contacto').on('show.bs.modal', function(e){
        console.log('Se muetra el modal  evento "show.bs.modal"');
        $('#btnContactoM').removeClass('btn-outline-primary');
        $('#btnContactoM').addClass('btn-success');
        $('#btnContactoM').prop('disabled', true);
    });
    $('#contacto').on('shown.bs.modal', function(e){
        console.log('Se muetra el modal, Espera carge css  evento "shown.bs.modal"');
    })
    $('#contacto').on('hide.bs.modal', function(e){
        console.log('Se oculta el modal  evento "hide.bs.modal"');
        $('#btnContactoM').addClass('btn-outline-primary');
        $('#btnContactoM').removeClass('btn-success');
        $('#btnContactoM').prop('disabled', false);
    });
    $('#contacto').on('hidden.bs.modal', function(e){
        console.log('Se oculta el modal, Espera carge css evento "hidden.bs.modal" ');
    })
    $('#contacto').on('hidePrevented.bs.modal', function(e){
        console.log('Se oculta el modal, evento "hidePrevented.bs.modal" ');
    });    

});